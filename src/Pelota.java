/**
 * Clase prototipo. Interfaz del objeto que se clona.
 * 
 * @author aitor_gc
 *
 */
public abstract class Pelota implements Cloneable {
	
	private String marca;
	private String deporte;
	private String color;
	private float radio;
	private float precio;
	
	
	public Pelota(String marca, String deporte, String color, float radio, float precio) {
		this.marca = marca;
		this.deporte = deporte;
		this.color = color;
		this.radio = radio;
		this.precio = precio;
	}
	
	


	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}




	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getDeporte() {
		return deporte;
	}


	public void setDeporte(String deporte) {
		this.deporte = deporte;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public float getRadio() {
		return radio;
	}


	public void setRadio(float radio) {
		this.radio = radio;
	}


	public float getPrecio() {
		return precio;
	}


	public void setPrecio(float precio) {
		this.precio = precio;
	}




	@Override
	public String toString() {
		return "Pelota [marca=" + marca + ", deporte=" + deporte + ", color=" + color + ", radio=" + radio + ", precio="
				+ precio + "]";
	}
	
	
	
	

}
