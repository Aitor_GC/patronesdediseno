/**
 * Programación Avanzada. Trabajo: Otros patrones de diseño.
 * 
 * Clase cliente. Encargada de pedirle los prototipos al gestor.
 * 
 * @author aitor_gc
 *
 */
public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {
		
		// Creamos una instancia del gestor de prototipos
		PelotaPrototype gestorPrototipos = new PelotaPrototype();
		
		// Solicitamos los prototimos
		Pelota pelotaBaloncesto = gestorPrototipos.prototipo(PelotaPrototype.Tipos.BALONCESTO);
		Pelota pelotaTenis = gestorPrototipos.prototipo(PelotaPrototype.Tipos.TENIS);
		Pelota pelotaFutbol = gestorPrototipos.prototipo(PelotaPrototype.Tipos.FUTBOL);

		System.out.println("Pelota baloncesto -> " + pelotaBaloncesto);
		System.out.println("Pelota tenis -> " + pelotaTenis);
		System.out.println("Pelota fútbol -> " + pelotaFutbol);

	}

}
