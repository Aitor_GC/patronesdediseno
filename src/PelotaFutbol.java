/**
 * Clase prototipo concreto.
 *  
 * @author aitor_gc
 *
 */
public class PelotaFutbol extends Pelota {

	public PelotaFutbol(String marca, String deporte, String color, float radio, float precio) {
		super(marca, deporte, color, radio, precio);
	}

}
