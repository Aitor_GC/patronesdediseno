import java.util.HashMap;

/**
 * Gestor de prototipos.
 * 
 * @author aitor_gc
 *
 */
public class PelotaPrototype {
	
	// Mapa de los diferentes prototimos creados
	private HashMap<Tipos, Pelota> prototipos = new HashMap<>();
	
	public PelotaPrototype() {
		prototipos.put(Tipos.BALONCESTO, new PelotaBaloncesto("Marca Baloncesto", "Baloncesto", "Naranja", 10, 30));
		prototipos.put(Tipos.FUTBOL, new PelotaBaloncesto("Marca Fútbol", "Fútbol", "Blanco", 8, 25));
		prototipos.put(Tipos.TENIS, new PelotaBaloncesto("Marca Tenis", "Tenis", "Amarillo", 3, 5));
	}
	
	public enum Tipos {
		BALONCESTO, FUTBOL, TENIS;
	}
	
	public Pelota prototipo(Tipos tipo) throws CloneNotSupportedException {
		return (Pelota) prototipos.get(tipo).clone();
	}
}
