/**
 * Clase prototipo concreto.
 * 
 * @author aitor_gc
 *
 */
public class PelotaBaloncesto extends Pelota {

	public PelotaBaloncesto(String marca, String deporte, String color, float radio, float precio) {
		super(marca, deporte, color, radio, precio);
	}

}
