/**
 * Clase prototipo concreto.
 * 
 * @author aitor_gc
 *
 */
public class PelotaTenis extends Pelota {

	public PelotaTenis(String marca, String deporte, String color, float radio, float precio) {
		super(marca, deporte, color, radio, precio);
	}

}
